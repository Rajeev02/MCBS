package com.example.rajeev.mcbs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AccountDetailActivity extends Activity {


EditText et1,et2,et3,et4,et5,et6,et7,et8,et9,et10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_detail_layout);


        et1=(EditText)findViewById(R.id.ad_accnum);
        et2=(EditText)findViewById(R.id.ad_cusnum);
        et3=(EditText)findViewById(R.id.ad_accholname);
        et4=(EditText)findViewById(R.id.ad_bank_name);
        et5=(EditText)findViewById(R.id.ad_branch_name);
        et6=(EditText)findViewById(R.id.ad_branch_address);
        et7=(EditText)findViewById(R.id.ad_branch_IFSC);
        et8=(EditText)findViewById(R.id.ad_MICR);
        et9=(EditText)findViewById(R.id.ad_current_balance);
        et10=(EditText)findViewById(R.id.ad_remark);

        Intent ref=getIntent();
        Bundle b=ref.getExtras();

       final String username=b.getString("k3");
        final String pass=b.getString("k4");

        try
        {
            SQLiteDatabase db=MainWelcome.mdb.getWritableDatabase();
            String qry="select * from account";
            Cursor cursor = db.rawQuery(qry,null);
            cursor.moveToFirst();
            do {
                String name=cursor.getString(2);
                String password=cursor.getString(10);

                if((name.equals(username))&&(password.equals(pass)))
                {
                    Toast.makeText(AccountDetailActivity.this, "You are in detail page", Toast.LENGTH_SHORT).show();
                    String acnumber = cursor.getString(0);
                    String custnum = cursor.getString(1);
                    String holdername = cursor.getString(2);
                    String bankname = cursor.getString(3);
                    String branchname = cursor.getString(4);
                    String branchaddress = cursor.getString(5);
                    String ifsc = cursor.getString(6);
                    String micr = cursor.getString(7);
                    String currentbal = cursor.getString(8);
                    String remark = cursor.getString(9);

                    et1.setText(acnumber);
                    et2.setText(custnum);
                    et3.setText(holdername);
                    et4.setText(bankname);
                    et5.setText(branchname);
                    et6.setText(branchaddress);
                    et7.setText(ifsc);
                    et8.setText(micr);
                    et9.setText(currentbal);
                    et10.setText(remark);

                    et1.setEnabled(false);
                    et2.setEnabled(false);
                    et3.setEnabled(false);
                    et4.setEnabled(false);
                    et5.setEnabled(false);
                    et6.setEnabled(false);
                    et7.setEnabled(false);
                    et8.setEnabled(false);
                    et9.setEnabled(false);
                    et10.setEnabled(false);
                }
            }while (cursor.moveToNext());
        }
        catch (Exception ex)
        {
            Log.e("Read Data",""+ex);
            Toast.makeText(AccountDetailActivity.this, "Reading Problem", Toast.LENGTH_SHORT).show();
        }

    }

    public void logOutAccount(View v)
    {
            startActivity(new Intent(this,LoginActivity.class));
            finish();
    }

    public void deleteAccount(View v)
    {
        Toast.makeText(AccountDetailActivity.this, "Under Development", Toast.LENGTH_SHORT).show();

    }

    public void transDetail(View v)
    {
        Toast.makeText(AccountDetailActivity.this, "Under Development", Toast.LENGTH_SHORT).show();


    }
    public void homePage(View v)
    {

        startActivity(new Intent(this,MainActivity.class));
        finish();
    }
}
