package com.example.rajeev.mcbs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddAccountActivity extends Activity {
    static boolean option = true;

    EditText et1, et2, et3, et4, et5, et6, et7, et8, et9, et10, et11, et12;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_account_layout);

        et1 = (EditText) findViewById(R.id.ana_accnum);
        et2 = (EditText) findViewById(R.id.ana_cusnum);
        et3 = (EditText) findViewById(R.id.ana_accholname);
        et4 = (EditText) findViewById(R.id.ana_bank_name);
        et5 = (EditText) findViewById(R.id.ana_branch_name);
        et6 = (EditText) findViewById(R.id.ana_branch_address);
        et7 = (EditText) findViewById(R.id.ana_branch_IFSC);
        et8 = (EditText) findViewById(R.id.ana_MICR);
        et9 = (EditText) findViewById(R.id.ana_current_balance);
        et10 = (EditText) findViewById(R.id.ana_remark);
        et11 = (EditText) findViewById(R.id.ana_password);
        et12 = (EditText) findViewById(R.id.ana_c_password);
        button=(Button)findViewById(R.id.addbutton);



        Intent ref = getIntent();
        Bundle b = ref.getExtras();

        final String ananame = b.getString("k3");
        final String anapass = b.getString("k4");
        et3.setText(ananame);
        et11.setText(anapass);

        et3.setEnabled(false);
        et11.setEnabled(false);

        try {

            SQLiteDatabase db = MainWelcome.mdb.getWritableDatabase();
            String qry = "select * from account";
            Cursor cursor = db.rawQuery(qry, null);
            cursor.moveToFirst();
            do {

                String name = cursor.getString(2);
                String password = cursor.getString(10);





                if((name.equals(ananame)) && (password.equals(anapass)))
                {
                    String acnumber = cursor.getString(0);
                    String custnum = cursor.getString(1);
                    String holdername = cursor.getString(2);
                    String bankname = cursor.getString(3);
                    String branchname = cursor.getString(4);
                    String branchaddress = cursor.getString(5);
                    String ifsc = cursor.getString(6);
                    String micr = cursor.getString(7);
                    String currentbal = cursor.getString(8);
                    String remark = cursor.getString(9);
                    if(acnumber.equals(""))
                    {
                       // Toast.makeText(AddAccountActivity.this, "Please fill the form", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        et1.setText(acnumber);
                        et2.setText(custnum);
                        et3.setText(holdername);
                        et4.setText(bankname);
                        et5.setText(branchname);
                        et6.setText(branchaddress);
                        et7.setText(ifsc);
                        et8.setText(micr);
                        et9.setText(currentbal);
                        et10.setText(remark);
                        et12.setText("........");


                        et1.setEnabled(false);
                        et2.setEnabled(false);
                        et3.setEnabled(false);
                        et4.setEnabled(false);
                        et5.setEnabled(false);
                        et6.setEnabled(false);
                        et7.setEnabled(false);
                        et8.setEnabled(false);
                        et9.setEnabled(false);
                        et10.setEnabled(false);
                        et12.setEnabled(false);
                        option=false;
                        break;
                    }
                }
            } while (cursor.moveToNext());

        } catch (Exception e) {

            Log.e("AddAccount Exc", "" + e);
        }
    }
    public void addNewAccount(View v) {
    try {

        if(option) {
            String acnumber = et1.getText().toString().trim();
            String custnum = et2.getText().toString().trim();
            String holdername = et3.getText().toString().trim();
            String bankname = et4.getText().toString().trim();
            String branchname = et5.getText().toString().trim();
            String branchaddress = et6.getText().toString().trim();
            String ifsc = et7.getText().toString().trim();
            String micr = et8.getText().toString().trim();
            String currentbal = et9.getText().toString().trim();
            String remark = et10.getText().toString().trim();
            String password = et11.getText().toString().trim();
            String cpass = et12.getText().toString().trim();


            if (password.equals(cpass)) {

              //  Toast.makeText(AddAccountActivity.this, "Password Matched", Toast.LENGTH_SHORT).show();
                boolean res = MainWelcome.mdb.addaccountdata(acnumber, custnum, holdername, bankname, branchname, branchaddress, ifsc, micr, currentbal, remark, password);

                if (res) {
                    Toast.makeText(AddAccountActivity.this, "Data Inserted", Toast.LENGTH_SHORT).show();

                    et1.setText("");
                    et2.setText("");
                    et3.setText("");
                    et4.setText("");
                    et5.setText("");
                    et6.setText("");
                    et7.setText("");
                    et8.setText("");
                    et9.setText("");
                    et10.setText("");
                    et11.setText("");
                    et12.setText("");

                } else {
                    Toast.makeText(AddAccountActivity.this, "Data not insert", Toast.LENGTH_SHORT).show();

                }
            }
        }
        else
        {
            Toast.makeText(AddAccountActivity.this, "You already registered", Toast.LENGTH_SHORT).show();
        }
    }catch (Exception ex) {

            Log.e("account Exception", "" + ex);
        }
    }
}
