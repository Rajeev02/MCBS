package com.example.rajeev.mcbs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;
public class AddTransaction extends Activity implements AdapterView.OnItemSelectedListener {
    Button b;
    EditText d,m,y, am, cn, cp, cd, re, accn;
    Spinner spinner;
    EditText et;
    String name;
    String select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_transaction_layout);
        spinner = (Spinner) findViewById(R.id.spinner);
        accn = (EditText) findViewById(R.id.ac_num);
        b = (Button) findViewById(R.id.acctype);
        d = (EditText) findViewById(R.id.d);
        m = (EditText) findViewById(R.id.m);
        y = (EditText) findViewById(R.id.y);

        am = (EditText) findViewById(R.id.tran_amt);
        cn = (EditText) findViewById(R.id.tra_check_no);
        cp = (EditText) findViewById(R.id.tra_check_party);
        cd = (EditText) findViewById(R.id.tran_check_detail);
        re = (EditText) findViewById(R.id.tran_remark);
        et = (EditText) findViewById(R.id.ac_num);
        spinner.setOnItemSelectedListener(this);
        loadSpinnerData();
        addListenerOnSpinnerItemSelection();
    }

    private void loadSpinnerData() {
        MyDB db = new MyDB(getApplicationContext());
        List<String> lables = db.getAllLabels();
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lables);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String label = parent.getItemAtPosition(position).toString();
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "" + label,
                Toast.LENGTH_LONG).show();
        Toast.makeText(parent.getContext(), "You selected: " + label,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void addListenerOnSpinnerItemSelection() {
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    public void addTransaction(View v) {
        try {

            String type = select;
            String account = accn.getText().toString().trim();
            String amount = am.getText().toString().trim();
            String checno = cn.getText().toString().trim();
            String checparty = cp.getText().toString().trim();
            String checdetail = cd.getText().toString().trim();
            String remark = re.getText().toString().trim();
            String day = d.getText().toString().trim();
            String month = m.getText().toString().trim();
            String year = y.getText().toString().trim();

/*
            if (type.equals("Withdraw")) {
                try {
                    SQLiteDatabase db = MainWelcome.mdb.getWritableDatabase();
                    String qry = "select * from account";
                    String qry1="select * from tran";
                    Cursor cursor = db.rawQuery(qry, null);
                    cursor.moveToFirst();
                    do {
                        String cbal = cursor.getString(8);
                        double cbalance=Double.parseDouble(cbal);

                        } while (cursor.moveToNext());

                    Cursor cursor1 = db.rawQuery(qry1, null);
                    cursor.moveToFirst();
                    do {
                        String cbal1 = cursor1.getString(3);
                        double cbalance1=Double.parseDouble(cbal1);

                        boolean res = MainWelcome.mdb.add_transaction(account, type, mydate, amount, checno, checparty, checdetail, remark);
                    } while (cursor1.moveToNext());


                } catch (Exception ex) {
                    Log.e("Read Data", "" + ex);
                    Toast.makeText(AddTransaction.this, "Reading Problem", Toast.LENGTH_SHORT).show();
                }

            }

        }
        else if (type.equals("Deposite")) {

        }

         else {
        }

        */
        boolean res = MainWelcome.mdb.add_transaction(account, type, day,month,year, amount, checno, checparty, checdetail, remark);
        Toast.makeText(AddTransaction.this, "Transaction In Process...", Toast.LENGTH_SHORT).show();
        if (res) {
            Toast.makeText(AddTransaction.this, "Transaction Data Inserted", Toast.LENGTH_SHORT).show();
            d.setText("");
            m.setText("");
            y.setText("");
            am.setText("");
            cn.setText("");
            cp.setText("");
            cd.setText("");
            re.setText("");
        } else {
            Toast.makeText(AddTransaction.this, "Transaction Data not insert", Toast.LENGTH_SHORT).show();

        }}


    catch(
    Exception ex)

    {
        Log.e("Transaction Exception", "" + ex);
    }
}
    public void pickDate(View v) {
        Calendar c = Calendar.getInstance();
       int date = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);


        final EditText dd = (EditText) findViewById(R.id.d);
        final EditText mm = (EditText) findViewById(R.id.m);
        final EditText yy = (EditText) findViewById(R.id.y);
        DatePickerDialog dpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dd.setText(""+dayOfMonth);
                mm.setText(""+(monthOfYear + 1));
                yy.setText(""+year);
            }
        }, year, month, date);
        dpd.show();


    }
    public void showType(View v) {
        String option[] = {"Withdraw", "Deposite"};
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle("Select One");
        ad.setCancelable(false);
        ad.setSingleChoiceItems(option, 2, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        b.setText("Withdraw");
                        select = "Withdraw";
                        break;
                    case 1:
                        b.setText("Deposite");
                        select = "Deposite";
                }
                dialog.cancel();
            }
        });
        ad.show();
    }
    public class CustomOnItemSelectedListener implements android.widget.AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            Toast.makeText(parent.getContext(), "Welcome :" + parent.getItemAtPosition(pos).toString(), Toast.LENGTH_SHORT).show();
            String name = "" + parent.getItemAtPosition(pos).toString();
            try {
                SQLiteDatabase db = MainWelcome.mdb.getWritableDatabase();
                String qry = "select * from account";
                Cursor cursor = db.rawQuery(qry, null);
                cursor.moveToFirst();
                do {
                    String n = cursor.getString(2);
                    if (n.equals(name)) {
                        final String acr = cursor.getString(0);
                        et.setText(acr);
                        break;
                    }
                } while (cursor.moveToNext());
            } catch (Exception e) {
                Log.e("AddAccount Exc", "" + e);
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }
}