package com.example.rajeev.mcbs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by rajeev on 4/3/16.
 */
public class AdminWelcome extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adminwelcome);

    }


    public void registerdUser(View v)
    {
        startActivity(new Intent(this, ViewRegisterAccount.class));

    }

    public  void accountList(View v)
    {

        startActivity(new Intent(this,ViewAccountList.class));

    }
    public void viewFeedback(View v)
    {
        startActivity(new Intent(this,ViewFeedback.class));
    }
    public void viewTransaction(View v)
    {
        startActivity(new Intent(this,ViewTransaction.class));
    }




}
