package com.example.rajeev.mcbs;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by rajeev on 5/3/16.
 */
public class FeedBackActivity extends Activity {
    LinearLayout ll;
    EditText et1, et2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_layout);
        ll=(LinearLayout)findViewById(R.id.ll4);
        et1 = (EditText) findViewById(R.id.myfeed);
        et2 = (EditText) findViewById(R.id.myfeeddata);


        try
        {
            SQLiteDatabase db=MainWelcome.mdb.getWritableDatabase();
            String qry="select * from suggestion";

            Cursor cursor = db.rawQuery(qry,null);
            cursor.moveToFirst();
            do {
                String name=cursor.getString(0);
                String data=cursor.getString(1);


                TextView tv = new TextView(this);
                tv.setText(name+" - "+data);
                tv.setTextSize(20);
                tv.setTextColor(Color.GREEN);
                ll.addView(tv);
                TextView tv1 = new TextView(this);
                tv1.setText("------------------------------------------------");
                tv1.setTextSize(20);
                tv1.setTextColor(Color.RED);
                ll.addView(tv1);
            }while (cursor.moveToNext());
        }
        catch (Exception ex)
        {
            Log.e("Read Data",""+ex);
            Toast.makeText(FeedBackActivity.this, "Reading Problem", Toast.LENGTH_SHORT).show();
        }

    }





   public void submitFeedBack(View v)
   {
       try {
           String myName = et1.getText().toString().trim();
           String myFeed = et2.getText().toString().trim();


           boolean res = MainWelcome.mdb.feedBack(myName,myFeed);
           Toast.makeText(FeedBackActivity.this, "Submission on process...", Toast.LENGTH_SHORT).show();
           if (res) {
               Toast.makeText(FeedBackActivity.this, "Data Inserted", Toast.LENGTH_SHORT).show();
               et1.setText("");
               et2.setText("");
           } else {
               Toast.makeText(FeedBackActivity.this, "Data not insert "+res, Toast.LENGTH_SHORT).show();

           }
       }
       catch (Exception ex)
       {
           Log.e("Feedback Exception",""+ex);
       }

   }
}
