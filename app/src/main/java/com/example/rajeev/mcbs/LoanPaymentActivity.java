package com.example.rajeev.mcbs;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class LoanPaymentActivity extends Activity {
    EditText et1,et2,et3;
    TextView tv1,tv2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loan_payment_layout);

        et1=(EditText)findViewById(R.id.lpl_lamt);
        et2=(EditText)findViewById(R.id.lpl_irate);
        et3=(EditText)findViewById(R.id.lpl_mnt);
        tv1=(TextView)findViewById(R.id.lpl_mp);
        tv2=(TextView)findViewById(R.id.lpl_tp);
    }
    public void loanCal(View v)
    {
        double amount =Integer.parseInt(et1.getText().toString());
        double rate =Integer.parseInt(et2.getText().toString());
        double month=Integer.parseInt(et3.getText().toString());
        double r=rate/1200;
        double r1=Math.pow(r+1,month);
        double  mp=((r+(r/(r1-1)))*amount);
        double tp=(mp*month);
        String monthly=(new DecimalFormat("##.##").format(mp));
        String total=(new DecimalFormat("##.##").format(tp));
        tv1.setText(monthly);
        tv2.setText(total);

    }
}
