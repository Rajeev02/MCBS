package com.example.rajeev.mcbs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

    EditText et1,et2;
    String defuser="Admin";
    String defpass="admin";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        et1=(EditText)findViewById(R.id.l_et1);
        et2=(EditText)findViewById(R.id.l_et2);
    }

    public void loginMain(View v) {

        String username=  et1.getText().toString().trim();
        String pass=et2.getText().toString().trim();

        if ((username.equals(defuser)) && (pass.equals(defpass))) {

            Toast.makeText(LoginActivity.this, "Welcome Admin", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, AdminWelcome.class));
           finish();
        }
        else {

            try {
                SQLiteDatabase db = MainWelcome.mdb.getWritableDatabase();
                String qry = "select * from register";
                Cursor cursor = db.rawQuery(qry, null);
                cursor.moveToFirst();
               int j=1;
                do {
                    String name = cursor.getString(0);
                    String password = cursor.getString(1);
                    if ((username.equals(name)) && (pass.equals(password))) {
                        Toast.makeText(LoginActivity.this, "Welcome " + name, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        i.putExtra("k1", name);
                        i.putExtra("k2", password);
                         j=0;
                        startActivity(i);
                        finish();
                        break;
                    }
                } while (cursor.moveToNext());

             int k=0;
                if(j!=k)
                {
                    Toast.makeText(LoginActivity.this, "Please Registered First or Check User Name and Password", Toast.LENGTH_SHORT).show();
                }


            } catch (Exception ex) {
                Log.e("Read Data", "" + ex);
            }
        }
    }


    public void registrationMain(View v)
    {
        startActivity(new Intent(this, RegistrationActivity.class));


    }
    public void backToHomePage(View v)
    {
        startActivity(new Intent(this,MainWelcome.class));
        finish();
    }
}
