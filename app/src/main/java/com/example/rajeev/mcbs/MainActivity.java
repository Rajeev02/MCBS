package com.example.rajeev.mcbs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_welcome);


    }

  /*  public void  loginAccount(View v)
    {
        startActivity(new Intent(this,LoginActivity.class));
    }
*/
    public void addAccount(View v)
    {
        Intent ref=getIntent();
        Bundle b=ref.getExtras();
        final String yname=b.getString("k1");
        final String ypass=b.getString("k2");
        Intent i=new Intent(MainActivity.this,AddAccountActivity.class);
        i.putExtra("k3",yname);
        i.putExtra("k4",ypass);
        startActivity(i);

    }

    public void addTransaction(View v)
    {
        startActivity(new Intent(this,AddTransaction.class));
    }

    public void recentTransaction(View v)
    {
        startActivity(new Intent(this,RecentTransactionActivity.class));
    }

    public void showLoanPayment(View v)
    {
        startActivity(new Intent(this,LoanPaymentActivity.class));
    }

    public void accountDetails(View v)
    {

        Intent ref=getIntent();
        Bundle b=ref.getExtras();
        final String yname=b.getString("k1");
        final String ypass=b.getString("k2");
            Intent i=new Intent(MainActivity.this,AccountDetailActivity.class);
             i.putExtra("k3",yname);
              i.putExtra("k4",ypass);
              startActivity(i);

    }

    public void notification(View v)
    {
        startActivity(new Intent(this,ViewNotification.class));
    }
    public void feedbackForm(View v)
    {
        startActivity(new Intent(this,FeedBackActivity.class));

    }
    public void aboutUs(View v)
    {
        startActivity(new Intent(this,AboutUs.class));

    }

    public void exitButton(View v)
    {
        Toast.makeText(MainActivity.this, "Thank you for using MCBS App :)", Toast.LENGTH_SHORT).show();
        finish();

    }



}
