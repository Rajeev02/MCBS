package com.example.rajeev.mcbs;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by rajeev on 3/3/16.
 */
    public class MyDB extends SQLiteOpenHelper {
    public static final String DB_NAME = "DB";
    public static final int VERSION = 1;
    public MyDB(Context context) {
        super(context, DB_NAME, null, VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            String qry = "create table register(user_name TEXT, user_password TEXT, user_email TEXT, user_contact TEXT)";
            String trans = "create table tran(account TEXT,option TEXT,day TEXT,month TEXT,year TEXT,amount TEXT, check_no TEXT, check_party TEXT, check_detail TEXT, remark TEXT)";
            String ft= "create table suggestion(username TEXT, data TEXT)";
            String ac = "create table account(ac_no TEXT, cu_no TEXT, ac_hol_name TEXT, bank_name TEXT, branch_name TEXT, branch_add TEXT, ifsc TEXT, micr TEXT, current_bal TEXT,remark TEXT, pass TEXT)";
            db.execSQL(qry);
            db.execSQL(ac);
            db.execSQL(trans);
            db.execSQL(ft);
        } catch (Exception ex) {
            Log.e("Table Create EXC", "" + ex);
        }
    }
    public boolean registerdata(String name, String password, String email, String contact) {
        try {
            String qry = "insert into register values(" + "'" + name + "','" + password + "','" + email + "','" + contact + "'" + ")";
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL(qry);
            return true;
        } catch (Exception ex) {
            Log.e("Insert Exception", "" + ex);
            return false;
        }
    }
    public boolean addaccountdata(String ac_no, String cu_no, String ac_hol_name, String bank_name, String branch_name, String branch_add, String ifsc, String micr, String current_bal, String remark, String passw) {
        try {
            String dt = "insert into account values(" + "'" + ac_no + "','" + cu_no + "','" + ac_hol_name + "','" + bank_name + "','" + branch_name + "','" + branch_add + "','" + ifsc + "','" + micr + "','" + current_bal + "','" + remark + "','" +passw + "'" + ")";
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL(dt);
            return true;
        } catch (Exception ex) {
            Log.e("Insert Exception", "" + ex);
            return false;
        }
    }
    public boolean add_transaction(String account_type, String option, String d,String m,String y, String amount, String check_no, String check_party, String check_detail, String remark) {

        try {
            String tr = "insert into tran values(" + "'" + account_type + "','" + option + "','" +d + "','" +m + "','"+y + "','"+ amount + "','" + check_no + "','" + check_party + "','" + check_detail + "','" + remark + "'" + ")";
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL(tr);
            return true;
        } catch (Exception ex) {
            Log.e("Insert tra Exception", "" + ex);
            return false;
        }
    }

    public boolean feedBack(String un,String fd)
    {
      try {
          String fbe = "insert into suggestion values(" + "'" + un + "','" + fd + "'" + ")";
          SQLiteDatabase db = getWritableDatabase();
          db.execSQL(fbe);
          return true;
      }
      catch (Exception ex)
      {
          Log.e("Feedback",""+ex);
          return false;
      }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS account");
        // Create tables again
        onCreate(db);
    }
    public List<String> getAllLabels(){
        List<String> labels = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM account";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        // closing connection
        cursor.close();
        db.close();
        // returning labels
        return labels;
    }
}
