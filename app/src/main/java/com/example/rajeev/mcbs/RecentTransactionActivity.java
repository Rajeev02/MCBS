package com.example.rajeev.mcbs;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RecentTransactionActivity extends Activity {
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recent_transaction_layout);

        ll=(LinearLayout)findViewById(R.id.ll2);

        try
        {
            SQLiteDatabase db=MainWelcome.mdb.getWritableDatabase();
            String qry="select * from tran";

            Cursor cursor = db.rawQuery(qry,null);
            cursor.moveToFirst();

            do {
                String account=cursor.getString(0);
                String type=cursor.getString(1);
                String date=cursor.getString(2);
                String amount=cursor.getString(3);



                TextView tv = new TextView(this);
                tv.setText(date+"     "+amount+"      "+account+"      "+type);
                tv.setTextSize(15);
                tv.setTextColor(Color.GREEN);
                ll.addView(tv);
                TextView tv1 = new TextView(this);
                tv1.setText("----------------------------------------------------------");
                tv1.setTextSize(20);
                tv1.setTextColor(Color.RED);
                ll.addView(tv1);
            }while (cursor.moveToNext());
        }
        catch (Exception ex)
        {
            Log.e("Read Data",""+ex);
            Toast.makeText(RecentTransactionActivity.this, "Reading Problem", Toast.LENGTH_SHORT).show();
        }

    }

/*    public void search(View v)
    {


        try
        {
            SQLiteDatabase db=MainWelcome.mdb.getWritableDatabase();
            String qry="select * from tran";

            Cursor cursor = db.rawQuery(qry,null);
            cursor.moveToFirst();

            do {
                String account=cursor.getString(0);
                String type=cursor.getString(1);
                String date=cursor.getString(2);
                String amount=cursor.getString(3);



                TextView tv = new TextView(this);
                tv.setText(date+"     "+amount+"      "+account+"      "+type);
                tv.setTextSize(12);
                tv.setTextColor(Color.GREEN);
                ll.addView(tv);
                TextView tv1 = new TextView(this);
                tv1.setText("----------------------------------------------------------");
                tv1.setTextSize(20);
                tv1.setTextColor(Color.RED);
                ll.addView(tv1);
            }while (cursor.moveToNext());
        }
        catch (Exception ex)
        {
            Log.e("Read Data",""+ex);
            Toast.makeText(RecentTransactionActivity.this, "Reading Problem", Toast.LENGTH_SHORT).show();
        }

    }


    public void fromDate(View v)
    {

        Calendar c= Calendar.getInstance();
        int date=c.get(Calendar.DAY_OF_MONTH);
        int month=c.get(Calendar.MONTH);
        int year=c.get(Calendar.YEAR);
        final EditText nw=(EditText)findViewById(R.id.t_from_date);

        DatePickerDialog dpd=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                nw.setText(dayOfMonth+" - "+(monthOfYear+1)+" - "+year);

            }
        },year,month,date);
        dpd.show();
    }
    public void toDate(View v)
    {

        Calendar c= Calendar.getInstance();
        int date=c.get(Calendar.DAY_OF_MONTH);
        int month=c.get(Calendar.MONTH);
        int year=c.get(Calendar.YEAR);
        final EditText nw=(EditText)findViewById(R.id.t_to_date);

        DatePickerDialog dpd=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                nw.setText(dayOfMonth+" - "+(monthOfYear+1)+" - "+year);
             }
        },year,month,date);
        dpd.show();
    }
*/
}
