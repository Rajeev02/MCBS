package com.example.rajeev.mcbs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationActivity extends Activity {

    EditText et1, et2, et3, et4, et5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);

        et1 = (EditText) findViewById(R.id.reg_user_name);
        et2 = (EditText) findViewById(R.id.reg_pass);
        et3 = (EditText) findViewById(R.id.reg_email_id);
        et4 = (EditText) findViewById(R.id.reg_cnt_number);
        et5 = (EditText) findViewById(R.id.reg_cpass);
    }


    public void register(View v)
    {
        boolean result = true;
        boolean match=true;
        try {
            String username = et1.getText().toString().trim();
            String password = et2.getText().toString().trim();
            String email = et3.getText().toString().trim();
            String contact = et4.getText().toString().trim();
            String cnfpass = et5.getText().toString().trim();

            try
            {
                SQLiteDatabase db=MainWelcome.mdb.getWritableDatabase();
                String qry="select * from register";

                Cursor cursor = db.rawQuery(qry,null);
                cursor.moveToFirst();
                do {
                    String name=cursor.getString(0);
                    String pas=cursor.getString(1);
                    if((username.equals(name))&&(pas.equals(password)))
                    {
                        result=false;
                        Toast.makeText(RegistrationActivity.this, ""+name+" you already Registered", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }while (cursor.moveToNext());

                if(result)
                {
                    Toast.makeText(RegistrationActivity.this, "Please Register First", Toast.LENGTH_SHORT).show();

                }
            }



            catch (Exception ex)
            {
                Log.e("Read Data",""+ex);
            }



            if ((password.equals(cnfpass))&&(match==result)){

                boolean res = MainWelcome.mdb.registerdata(username, password, email, contact);

                if (res) {
                    Toast.makeText(RegistrationActivity.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                    et1.setText("");
                    et2.setText("");
                    et3.setText("");
                    et4.setText("");
                    et5.setText("");
                    startActivity(new Intent(this,LoginActivity.class));
                    finish();
                } else {
                    Toast.makeText(RegistrationActivity.this, "Data not insert", Toast.LENGTH_SHORT).show();

                }

            } else {

                Toast.makeText(RegistrationActivity.this, "Registration Failed", Toast.LENGTH_SHORT).show();

            }
        }
        catch (Exception ex)
        {
            Log.e("Invalid Register",""+ex);
            Toast.makeText(RegistrationActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
        }

    }
}


