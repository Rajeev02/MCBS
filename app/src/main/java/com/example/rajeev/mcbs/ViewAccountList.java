package com.example.rajeev.mcbs;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by rajeev on 4/3/16.
 */
public class ViewAccountList extends Activity {

    LinearLayout ll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewaccountlist);

        ll=(LinearLayout)findViewById(R.id.ll2);

        try
        {
            SQLiteDatabase db=MainWelcome.mdb.getWritableDatabase();
            String qry="select * from account";

            Cursor cursor = db.rawQuery(qry,null);
            cursor.moveToFirst();
            do {
                String name=cursor.getString(0);
                String password=cursor.getString(1);
                String email=cursor.getString(2);
                String contact=cursor.getString(3);


                String acnumber = cursor.getString(0);
                String custnum = cursor.getString(1);
                String holdername =cursor.getString(2);
                String bankname = cursor.getString(3);
                String branchname=cursor.getString(4);
                String branchaddress =cursor.getString(5);
                String ifsc= cursor.getString(6);
                String micr = cursor.getString(7);
                String currentbal = cursor.getString(8);
                String remark= cursor.getString(9);


                TextView tv = new TextView(this);
                tv.setText(acnumber+" - "+custnum+" - "+holdername+" - "+bankname+" - "+branchname+" - "+branchaddress+" - "+ifsc+" - "+micr+" - "+currentbal+" - "+remark);
                tv.setTextSize(20);
                tv.setTextColor(Color.BLUE);
                ll.addView(tv);
            }while (cursor.moveToNext());
        }
        catch (Exception ex)
        {
            Log.e("Read Data",""+ex);
            Toast.makeText(ViewAccountList.this, "Reading Problem", Toast.LENGTH_SHORT).show();
        }

    }
}
