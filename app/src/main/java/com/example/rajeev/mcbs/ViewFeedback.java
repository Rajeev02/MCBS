package com.example.rajeev.mcbs;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by rajeev on 4/3/16.
 */
public class ViewFeedback extends Activity {

    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewfeedback);

        ll=(LinearLayout)findViewById(R.id.vfb);


        try
        {
            SQLiteDatabase db=MainWelcome.mdb.getWritableDatabase();
            String qry="select * from suggestion";

            Cursor cursor = db.rawQuery(qry,null);
            cursor.moveToFirst();
            do {
                String name=cursor.getString(0);
                String data=cursor.getString(1);


                TextView tv = new TextView(this);
                tv.setText(name+" - "+data);
                tv.setTextSize(20);
                tv.setTextColor(Color.GREEN);
                ll.addView(tv);
                TextView tv1 = new TextView(this);
                tv1.setText("------------------------------------------------");
                tv1.setTextSize(20);
                tv1.setTextColor(Color.RED);
                ll.addView(tv1);
            }while (cursor.moveToNext());
        }
        catch (Exception ex)
        {
            Log.e("Read Data",""+ex);
            Toast.makeText(ViewFeedback.this, "Reading Problem", Toast.LENGTH_SHORT).show();
        }

    }

}

