package com.example.rajeev.mcbs;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by rajeev on 2/3/16.
 */
    public class ViewRegisterAccount extends Activity {

    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewregisteraccount);

        ll=(LinearLayout)findViewById(R.id.ll1);

        try
        {
            SQLiteDatabase db=MainWelcome.mdb.getWritableDatabase();
            String qry="select * from register";

            Cursor cursor = db.rawQuery(qry,null);
            cursor.moveToFirst();
            do {
                String name=cursor.getString(0);
                String password=cursor.getString(1);
                String email=cursor.getString(2);
                String contact=cursor.getString(3);

                TextView tv = new TextView(this);
                tv.setText(name+" - "+password+" - "+email+" - "+contact);
                tv.setTextSize(20);
                tv.setTextColor(Color.RED);
                ll.addView(tv);
            }while (cursor.moveToNext());
        }
        catch (Exception ex)
        {
            Log.e("Read Data",""+ex);
            Toast.makeText(ViewRegisterAccount.this, "Reading Problem", Toast.LENGTH_SHORT).show();
        }

    }
}
