package com.example.rajeev.mcbs;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by rajeev on 4/3/16.
 */
public class ViewTransaction extends Activity {

    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewtransaction);

        ll=(LinearLayout)findViewById(R.id.vtl);

        try
        {
            SQLiteDatabase db=MainWelcome.mdb.getWritableDatabase();
            String qry="select * from tran";

            Cursor cursor = db.rawQuery(qry,null);
            cursor.moveToFirst();
            do {
                String amount=cursor.getString(0);
                String checno=cursor.getString(1);
                String checparty=cursor.getString(2);
                String checdetail=cursor.getString(3);
                String remark=cursor.getString(4);
                String day=cursor.getString(5);
                String month=cursor.getString(6);
                String year=cursor.getString(7);


                TextView tv = new TextView(this);
                tv.setText(amount+" - "+checno+" - "+checparty+" - "+checdetail+" - "+remark+"-"+day+"-"+month+"-"+year);
                tv.setTextSize(20);
                tv.setTextColor(Color.GREEN);
                ll.addView(tv);
               TextView tv1 = new TextView(this);
                tv1.setText("------------------------------------------------");
                tv1.setTextSize(20);
                tv1.setTextColor(Color.RED);
                ll.addView(tv1);
            }while (cursor.moveToNext());
        }
        catch (Exception ex)
        {
            Log.e("Read Data",""+ex);
            Toast.makeText(ViewTransaction.this, "Reading Problem", Toast.LENGTH_SHORT).show();
        }

    }

}
